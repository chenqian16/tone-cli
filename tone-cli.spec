Name:           tone-cli
Version:        1.0
Release:        1
Summary:        tone client command line tools

License:        Mulan PSL v2
URL:            https://gitee.com/anolis/tone-cli.git
# Generate tar file by command:
# git archive --format=tar --prefix=tone-cli/ HEAD -o tone-cli.tar
Source0:        %{name}.tar

# On different distribution, condition detect is used.
# E.g:
# if "%{dist}" == "anolis"
#    Requires:  python3-pyyaml python3-psutil wget zip unzip gcc
# endif
Requires:       python3-pyyaml python3-psutil wget zip unzip gcc

%description
tone client command line tools

%prep
%autosetup -n %{name} -p1

%build

%install
mkdir -p %{buildroot}/opt/%{name}/
cp -r %{_builddir}/%{name}/{bin,conf,core,docs,etc,lib,tests} %{buildroot}/opt/%{name}/$d
install -Dpm0755 %{_builddir}/%{name}/tone %{buildroot}/opt/%{name}/tone
install -Dpm0655 %{_builddir}/%{name}/Makefile %{buildroot}/opt/%{name}/Makefile
install -Dpm0655 %{_builddir}/%{name}/LICENSE.txt %{buildroot}/opt/%{name}/LICENSE.txt
install -Dpm0655 %{_builddir}/%{name}/README.md %{buildroot}/opt/%{name}/README.md
install -Dpm0755 %{_builddir}/%{name}/README-zh_CN.md %{buildroot}/opt/%{name}/README-zh_CN.md
ln -s -f %{buildroot}/opt/%{name}/tone %{buildroot}%{_bindir}/tone

%files
/opt/%{name}
%{_bindir}/tone

%changelog
* Wed Jun 7 2023 Qiang Wei <qiang.wei@suse.com> - 1.0-1
- Init rpm build spec.
