#!/usr/bin/env python
# -*- coding: utf-8 -*-

from module_interfaces import TESTMODULE
# from utils.log import logger
from utils.tests import TestCommands


class Run(TESTMODULE):
    priority = 200

    def run(self, test_instance):
        testcmd = TestCommands(test_instance.config)
        testcmd.run()
