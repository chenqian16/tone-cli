#!/bin/env python3
# coding=utf-8

import argparse
import sys
import os
libpath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(libpath)
try:
    from disksetup import diskinfo
except Exception:
    raise


def to_byte(value):
    unit = value[-1]
    data = value[:-1]
    if unit in ('g', 'G'):
        return _g_to_b(data)
    if unit in ('k', 'K'):
        return _k_to_b(data)
    if unit in ('m', 'M'):
        return _m_to_b(data)
    if unit in ('b', 'B'):
        return data


def _m_to_b(value):
    return int(value) << 20


def _k_to_b(value):
    return int(value) << 10


def _g_to_b(value):
    return int(value) << 30


def gen_global(config):
    tasks = ["[global]"]
    args_used = [
        'bs',
        'ioengine',
        'iodepth',
        'direct',
        'runtime',
        'fallocate',
    ]
    for arg in args_used:
        tasks.append("{}={}".format(arg, config.__dict__[arg]))
    tasks.append("group_reporting")

    size = int(to_byte(config.test_size) / config.nr_task)
    tasks.append('size={}'.format(size))
    if config.nr_task > 0:
        tasks.append("thread=1")

    if config.ioengine == 'mmap':
        tasks.append("time_based=0")
    else:
        tasks.append("time_based=1")

    return tasks


def get_jobs_on_each_disk(config, device_number):
    if config.nr_task % device_number > 0:
        print("Error nr_task({}) %% device_number({}) >0".format(
            config.nr_task, device_number
        ))
        sys.exit(1)
    config.jobs_on_each_device = config.nr_task / device_number


def gen_tasks(config):
    tasks = []
    parts = diskinfo.get_test_parts()
    lvms = diskinfo.get_lvm_parts()
    if lvms:
        parts = diskinfo.get_test_parts()

    mps = diskinfo.get_test_mountpoints()
    if mps:
        check_disk_size(config, mps)
        get_jobs_on_each_disk(config, len(mps))
        for index, directory in enumerate(mps):
            tasks.extend(gen_task(config, index, directory))
    elif parts:
        check_disk_size(config, parts)
        get_jobs_on_each_disk(config, len(parts))
        for index, device in enumerate(parts):
            tasks.extend(gen_task(config, index, device, type="filename"))
    else:
        devices = disk.get_memory_disk(config.device)
        check_disk_size(config, devices)
        if devices:
            get_jobs_on_each_disk(config, len(devices))
            for index, device in enumerate(devices):
                tasks.extend(
                    gen_task(config, index, device, type="filename"))
        else:
            print("Error: Can not find used devices or directory")
            sys.exit(1)
    return tasks


def check_disk_size(config, devices):
    minimal_disk_size = sys.maxsize
    for device in devices:
        d = os.path.basename(device)
        minimal_disk_size = min(minimal_disk_size, diskinfo._disks[d]['size'])
    need_size = int(to_byte(config.test_size) / len(devices))
    if need_size > minimal_disk_size * 1024 * 1024 * 1024:
        print("Error: Need more disk size {}".format(need_size))
        sys.exit(1)


def gen_task(config, num, device, type="directory"):
    task = []
    task.append("[task_{}]".format(num))
    task.append("rw={}".format(config.rw))
    task.append("{}={}".format(type, device))
    task.append("numjobs={}".format(int(config.jobs_on_each_device)))
    return task


def main(config):
    tasks = []
    tasks.extend(gen_global(config))
    tasks.extend(gen_tasks(config))
    print("\n".join(tasks))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--bs',
        default='4k',
        help="default value: 4k"
    )
    parser.add_argument(
        '--ioengine',
        default='sync',
        help="default value: sync"
    )
    parser.add_argument(
        '--rw',
        default='write',
        help="default value: write"
    )
    parser.add_argument(
        '--iodepth',
        default='32',
        help="default value: 32"
    )
    parser.add_argument(
        '--direct',
        default='1',
        help="default value: 1"
    )
    parser.add_argument(
        '--runtime',
        default='120',
        help="default value: 120"
    )
    parser.add_argument(
        '--fallocate',
        default='native',
        help="default value: native"
    )
    parser.add_argument(
        '--nr_task',
        default=1,
        type=int,
        help="default value: 1"
    )
    parser.add_argument(
        '--test_size',
        required=True,
        help="Must be provided"
    )
    parser.add_argument(
        '--device',
        required=True,
        help="If device is mount then mount point directory is used"
    )

    config = parser.parse_args()
    main(config)
