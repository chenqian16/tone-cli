#!/bin/bash

run() {
  if [ "$function" == "dde_install" ]; then
    sed -i s/enabled=0/enabled=1/g /etc/yum.repos.d/AnolisOS-DDE.repo
    yum install dde -y
    if [ $? -eq 0 ]; then
      echo "====PASS: dde_install"
    else
      echo "====FAIL: dde_install"
    fi
  elif [ "$function" == "dde_check" ]; then
    test -f /etc/deepin/dde-session-ui.conf
    if [ $? -eq 0 ]; then
      echo "====PASS: dde_check"
    else
      echo "====FAIL: dde_check"
    fi
    yum remove dde -y
  elif [ "$function" == "dde_remove" ]; then
    sed -i s/enabled=1/enabled=0/g /etc/yum.repos.d/AnolisOS-DDE.repo
    test -f /etc/deepin/dde-session-ui.conf
    if [ $? -ne 0 ]; then
      echo "====PASS: dde_remove"
    else
      echo "====FAIL: dde_remove"
    fi
  fi
}

parse()
{
	$TONE_BM_SUITE_DIR/parse.awk
}