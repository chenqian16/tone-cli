GIT_URL="https://gitee.com/tengent/ltp.git"

BRANCH="master"


if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
         DEP_PKG_LIST="pkg-config libtirpc-dev autoconf m4 genisoimage sysstat automake gcc psmisc libaio-dev unzip dosfstools keyutils net-tools bc numactl libcap-dev quota"

else
         DEP_PKG_LIST="libtirpc-devel autoconf m4 genisoimage sysstat automake gcc psmisc libaio-devel unzip dosfstools keyutils net-tools bc numactl-devel libcap-devel quota"

fi


build()
{
	export CFLAGS+="-fcommon"
	make autotools
	./configure --prefix="$TONE_BM_RUN_DIR"
	make
}

install()
{
    make install
}
