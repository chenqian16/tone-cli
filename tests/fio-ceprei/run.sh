setup()
{
	echo ''    
}

run()
{

	  disk=$(df | awk 'NR == 1 {max = 1;next}{if (max < $4){disk = $6; max = $4}}END{print disk}')
    export PATH="$PATH:$TONE_BM_RUN_DIR/bin"
    local task_output=$TONE_CURRENT_RESULT_DIR/fio-ceprei.output
    task="-filename=$disk/data_file -name=${test} -ioengine=${ioengine} -time_based=${time_based} \
    -direct=${direct} -size=${size} -rw=${rw} -bs=${bs} -thread -iodepth=${iodepth} \
    -numjobs=${numjobs} -runtime=${runtime} -lockmem=${lockmem} -ramp_time=${ramp_time}"
    echo running----------
    if fio $task --output-format=json --output="$task_output"
    then 
    	echo running success----------
    else
    	echo running failed-----------
    fi
}

teardown()
{
	rm -rf $disk/data_file
}

parse()
{
    python $TONE_BM_SUITE_DIR/parse.py

}
