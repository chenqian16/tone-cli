# Avaliable environment:
#
# Download variable:Download variable
GIT_URL="https://gitee.com/anolis/ancert.git"
BM_NAME="ancert"
DEP_PKG_LIST="git make gcc gcc-c++ fio nvme-cli glx-utils python3 rpm-build bc lvm2 alsa-lib alsa-utils virt-what smartmontools hdparm OpenIPMI ipmitool freeipmi"

fetch()
{
    git_clone $GIT_URL $TONE_BM_CACHE_DIR
}

extract_src()
{
    :
}

build()
{
    local os_id="$(cat /etc/os-release|awk -F'"' '/^ID=/{print $(NF-1)}')"
    local dist_ver=$(rpm -E %{?dist}|sed -e 's/.an//g')

    test "${os_id}" = "anolis" || { echo "Error: Please build ancert kit in anolis os";exit 1; }
    # Support different Anolis OS versions
    if test "$dist_ver" -ge 23;then
        yum install -y xorg-x11-server-Xwayland x11perf xdpyinfo xvinfo xset python3-pyyaml clang
    else
        if test "$dist_ver" -ge 8;then
            yum install -y python3-pyyaml
        else
            yum repolist|grep -q epel || wget -O /etc/yum.repos.d/epel.repo https://mirrors.aliyun.com/repo/epel-7.repo
            test "$dist_ver" -ge 7 && {  yum install -y python*-PyYAML || pip3 install pyyaml; } || \
            { echo "Error getting dist version($dist_ver),it should be greater than 7.x";exit 1; }
        fi
        yum install -y xorg-x11-utils xorg-x11-server-utils xorg-x11-apps clang
    fi
}

install()
{
    [ -d "$TONE_BM_RUN_DIR/$BM_NAME" ] && rm -rf $TONE_BM_RUN_DIR/$BM_NAME
    cp -af $TONE_BM_CACHE_DIR $TONE_BM_RUN_DIR
}

uninstall()
{
    rm -rf $TONE_BM_RUN_DIR/$BM_NAME
    rm -rf $TONE_BM_CACHE_DIR
}