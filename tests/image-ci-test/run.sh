#!/bin/bash
# Avaliable environment variables:
# $C_VERSION: image version seperated by '#'
# $CONTAINER_ENGINE: container engine for func test seperated by '/'

group_array=$(cat $TONE_CONF_DIR/functional/image-ci-test.conf | grep -v ^group$)

run()
{
    [ -n "$group" ] || {
        echo "No group in specfile"
        return 1
    }
    
    [[ "$group_array" =~  "$group" ]] || {
        echo "$group is not supported"
        return 1
    }
    export PYTHONPATH=$TONE_BM_RUN_DIR/image_ci/tests
    registry_type=(`echo $REGISTRY_TYPE | tr '/' ' '`)
    if [ "$registry_type" == "app" ]; then
        export CONTAINER_ENGINE=docker
    fi

    option=""
    run_tags=(`echo $TONE_TESTCASES`)
    if [ -n "$run_tags" ] && [ "$run_tags" != "null" ] && [ "$run_tags" != "container_default" ]; then
        option="-t $run_tags"
    fi

    generate_yaml
    if [ "$group" == "container_startup_test" ]; then
        avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/image_ci/tests/smoke --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml -t container $option
    elif [ "$group" == "application_container_func_test" ]; then
        if [ "$registry_type" == "app" ]; then
            avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/image_ci/tests/smoke --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml -t app_container
        else
            echo "====SKIP:$group"
        fi
    elif [ "$group" == "keentune_application_container_func_test" ]; then
        if echo "$CONTAINER_SERVICE" |grep -q "optimized"; then
            service_name=${CONTAINER_SERVICE/optimized/keentune}
        else
            service_name=$CONTAINER_SERVICE
        fi
        avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/image_ci/tests/keentune/tc_container_${service_name}_001.py --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml
    else
        option="$option -t container_default"
        if [ "$group" != "container_default_group" ]; then
            option="$option -t ${group}_default"
        fi
        avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/image_ci/tests/smoke --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml $option
    fi
    cp -rH $HOME/avocado/job-results/latest $TONE_CURRENT_RESULT_DIR
}

parse()
{
    $TONE_BM_SUITE_DIR/parse.py
}

generate_yaml()
{
    registry_addr=(`echo $REGISTRY_ADDR | tr '#' ' '`)
    c_engine=(`echo $CONTAINER_ENGINE | tr '/' ' '`)
cat > hosts.yaml << EOF
hosts: !mux
    localhost:
        host: localhost
registry: !mux
EOF
    for ((i = 0; i < ${#registry_addr[@]}; i++)); do
cat >> hosts.yaml << EOF
    registry$i:
        version: ${registry_addr[$i]}
EOF
    done

cat >> hosts.yaml << EOF
engines: !mux
EOF
    for ((i = 0; i < ${#c_engine[@]}; i++)); do
cat >> hosts.yaml << EOF
    engine$i:
        engine: ${c_engine[$i]}
EOF
    done
}
