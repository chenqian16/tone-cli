WEB_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/iozone3_492.tgz"

build()
{
    cp -r $TONE_BM_CACHE_DIR/iozone3_492.tgz $TONE_BM_RUN_DIR
    cd $TONE_BM_RUN_DIR
    tar -xvf iozone3_492.tgz
    cd $TONE_BM_RUN_DIR/iozone3_492/src/current
    if [[ $(/bin/arch) == 'x86_64' ]];then
        make linux-AMD64
    else
        make linux
    fi
}

install()
{
    echo "iozone install successful"
}
