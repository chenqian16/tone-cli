#!/bin/bash

run()
{
	git status
	if [ -n "$PKG_CI_REPO_TARGET_BRANCH" ] && [ -n "$PKG_CI_REPO_TARGET_URL" ]; then
		echo -e "git merge target branch"
		echo "PKG_CI_REPO_TARGET_URL is $PKG_CI_REPO_TARGET_URL"
		echo "PKG_CI_REPO_TARGET_BRANCH is $PKG_CI_REPO_TARGET_BRANCH"
		git pull --no-edit $PKG_CI_REPO_TARGET_URL $PKG_CI_REPO_TARGET_BRANCH
		if [ $? -eq 0 ]; then
			git status
		else
			exit 1
		fi
	fi

	cd $TONE_BM_RUN_DIR/tests/bats
	make ci > $TONE_BM_RUN_DIR/ci_test.log 2>&1
}

teardown()
{
	/usr/bin/cp $TONE_BM_RUN_DIR/ci_test.log $TONE_CURRENT_RESULT_DIR/ || true
	/usr/bin/cp $TONE_BM_RUN_DIR/tests/bats/nydus-snapshotter*.log $TONE_CURRENT_RESULT_DIR/ || true
	/usr/bin/cp $TONE_BM_RUN_DIR/tests/bats/dmesg*.log $TONE_CURRENT_RESULT_DIR/ || true
}

parse()
{
	cat $TONE_BM_RUN_DIR/ci_test.log | grep -v '^#' | egrep '^(ok|not ok)' > result.log
	while read line
	do
		if echo $line|grep -q '^ok'; then
			testcase=$(echo $line | cut -d ' ' -f3-)
			echo "${testcase}: pass"
		elif echo $line|grep -q '^not ok'; then
			testcase=$(echo $line | cut -d ' ' -f4-)
			echo "${testcase}: fail"
		fi
	done < result.log
	[ -f result.log ] && rm -f result.log
}
