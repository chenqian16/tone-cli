# plugsched-tests
## Description
Integrate test cases of Plugsched project.

## Homepage
https://github.com/aliyun/plugsched

## Version
1.0

## Category
Functional

## Parameters
- bundle: test case bundles, such as ci

## Results

```
CPU_domain_rebuild_test: Pass
CPU_throttle_test: Pass
Public_vars_test: Pass
Var_uniformity_test: Pass
Sched_syscall_test: Pass
...
```
