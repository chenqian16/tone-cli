#!/bin/sh
#set -x

logfile=$1
[ -f "$logfile" ] || exit 1

# sed remove color codes
logdir=$(dirname $logfile)
ptslog="$logdir/pts.log"
sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" $logfile > $ptslog

# split log with pattern
# Args:
# - $1: str pattern
# - $2: output dir
split_log()
{
	[ -n "$1" ] || return 1
	[ -n "$2" ] || tmpdir=$(mktemp -d /tmp/ptslogs.XXXXXX)

	local pat=$1
	local idx=0
	local tmplog="$tmpdir/log.$idx"
	while read -r line
	do
		echo $line | grep -qE "$pat" && {
			idx=$((idx+1))
			tmplog="$tmpdir/log.$idx"
		}
		echo "$line" >> $tmplog
	done < $ptslog
}

# Common parser
common_parser()
{
	local name=$1
	local keyword=$2

	bench=$(grep "^$name" $ptslog | head -1 | sed -e 's/\ /-/g' -e 's/://g')
	split_log "$name.*:"
	for log in $tmpdir/log.*; do
		config=""
		result=""
		unit=""
		config=$(grep "$keyword" $log | tail -1 | sed -e 's/://g'  -e 's/,//g' -e 's/\ /-/g')
		result=$(grep 'Average:' $log | awk '{print $2}')
		unit=$(grep 'Average:' $log | awk '{$1=""; $2=""; print $0}' | sed -e 's/^\ *//g' -e 's/\ /-/g')
		[ -n "$config" ] && [ -n "$result" ] && echo "$bench-$config: $result $unit"
	done
}

# SQLite result parser 
parse_sqlite_result()
{
	bench=$(grep '^SQLite' $ptslog | head -1 | sed -e 's/\ /-/g' -e 's/://g')
	split_log "SQLite .*:"
	for log in $tmpdir/log.*; do
		threads=""
		config=""
		seconds=""
		threads=$(grep 'Threads / Copies:.*:' $log | sed 's/://g' | awk '{print $NF}' | xargs)
		[ -n "$threads" ] && config="Threads-$threads"
		seconds=$(grep "Average:.*Seconds" $log | awk '{print $2}')
		[ -n "$config" ] && [ -n "$seconds" ] && echo "$bench-$config: $seconds Seconds"
	done
}

# FIO result parser
parse_fio_result()
{
        bench=$(grep '^Flexible IO Tester' $ptslog | head -1 | sed -e 's/\ /-/g' -e 's/://g')
        split_log "Flexible IO Tester .*:"
        for log in $tmpdir/log.*; do
                config=""
                MBPS=""
                IOPS=""
                config=$(grep 'Type:.*Engine:.*' $log | tail -1  | awk -F ':' '{print $2,$6}' | sed -e 's/\ /-/g')
                MBPS=$(grep 'Average: .* MB/s' $log | awk '{print $2}')
                IOPS=$(grep 'Average: .* IOPS' $log | awk '{print $2}')
                [ -n "$config" ] && [ -n "$MBPS" ] && echo "Flexible-IO-Tester$config-MBPS: $MBPS MB/s"
                [ -n "$config" ] && [ -n "$IOPS" ] && echo "Flexible-IO-Tester$config-IOPS: $IOPS IOPS"
        done
}

# BlogBench result parser
parse_blogbench_result()
{
	bench=$(grep '^BlogBench' $ptslog | head -1 | sed -e 's/\ /-/g' -e 's/://g')
	split_log "BlogBench .*:"
	for log in $tmpdir/log.*; do
		config=""
		result=""
		config=$(grep -E 'Test: (Read|Write)' $log | tail -1 | sed -e 's/://g'  -e 's/,//g' -e 's/\ /-/g')
		result=$(grep 'Average: .* Score' $log | awk '{print $2}')
		[ -n "$config" ] && [ -n "$result" ] && echo "$bench-$config: $result Score"
	done
}

# 7-Zip Compression result parser
parse_7zip_result()
{
	bench=$(grep '^7-Zip Compression' $ptslog | head -1 | sed -e 's/\ /-/g' -e 's/://g')
	split_log "7-Zip Compression .*:"
	for log in $tmpdir/log.*; do
		compress_rate=""
		decompress_rate=""
		compress_rate=$(grep -A 6 "Test: Compression Rating"  $log | grep 'Average:' | awk '{print $2}')
		decompress_rate=$(grep -A 6 "Test: Decompression Rating"  $log | grep 'Average:' | awk '{print $2}')
		[ -n "$compress_rate" ] && echo "$bench-Test-Compression-Rating: $compress_rate MIPS"
		[ -n "$decompress_rate" ] && echo "$bench-Test-Decompression-Rating: $decompress_rate MIPS"
	done
}

# OpenSSL result parser
parse_openssl_result()
{
        bench=$(grep '^OpenSSL' $ptslog | head -1 | sed -e 's/\ /-/g' -e 's/://g')
        split_log "OpenSSL .*:"
        for log in $tmpdir/log.*; do
                signs=""
                signs=$(grep "Average:" $log | awk '{print $2}')
                [ -n "$signs" ] &&  echo "$bench-RSA-4096-bit-Performance: $signs Per-Second"
        done
}

# PostgreSQL result parser
parse_pgbench_result()
{
	bench=$(grep '^PostgreSQL' $ptslog | head -1 | sed -e 's/\ /-/g' -e 's/://g')
	split_log "PostgreSQL .*:"
	for log in $tmpdir/log.*; do
		config=""
		tps=""
		latency=""
		config=$(sed -n 's/^.*pts\/pgbench.*\[\(.*\)\]$/\1/p' $log | sed -e 's/://g'  -e 's/\ /-/g')
		[ -n "$config" ] || continue
		tps=$(grep "Average: .* TPS" $log | awk '{print $2}')
		latency=$(grep "Average: .* ms" $log | awk '{print $2}')
		[ -n "$tps" ] && echo "$bench-$config-TPS: $tps TPS"
		[ -n "$latency" ] && echo "$bench-$config-Latency: $latency ms"
	done
}

# process pts.log
grep -q '^SQLite' $ptslog && parse_sqlite_result
grep -q '^Flexible IO Tester' $ptslog && parse_fio_result
grep -q '^BlogBench' $ptslog && parse_blogbench_result
grep -q '^7-Zip Compression' $ptslog && parse_7zip_result
grep -q '^OpenSSL' $ptslog && parse_openssl_result
grep -q '^PostgreSQL' $ptslog && parse_pgbench_result
grep -q '^FS-Mark' $ptslog && common_parser "FS-Mark" "Test: .*Files,"
grep -q '^Dbench' $ptslog && common_parser "Dbench" "Client Count:"
grep -q '^IOzone' $ptslog && common_parser "IOzone" "Record Size:"
grep -q '^Threaded I/O Tester' $ptslog && common_parser "Threaded I/O Tester" "Thread Count:"
grep -q '^Compile Bench' $ptslog && common_parser "Compile Bench" "Test: .*:"
grep -q '^PostMark' $ptslog && common_parser "PostMark" "Disk Transaction Performance:"
grep -q '^RAMspeed SMP' $ptslog && common_parser "RAMspeed SMP" "Type: .* - Benchmark:"
grep -q '^Stream' $ptslog && common_parser "Stream" "Type: .*:"
grep -q '^Loopback TCP Network Performance' $ptslog && \
	common_parser "Loopback TCP Network Performance" "Time To Transfer 10GB Via Loopback:"
grep -q '^Timed MAFFT Alignment' $ptslog && common_parser "Timed MAFFT Alignment" "Multiple Sequence Alignment"
grep -q '^CacheBench' $ptslog && common_parser "CacheBench" "Test: .*:"
grep -q '^John The Ripper' $ptslog && common_parser "John The Ripper" "Test: .*:"
grep -q '^Gzip Compression' $ptslog && common_parser "Gzip Compression" "Linux Source Tree Archiving"
grep -q '^C-Ray' $ptslog && common_parser "C-Ray" "Total Time"
grep -q '^Sysbench' $ptslog && common_parser "Sysbench" "Test: .*:"
grep -q '^nginx' $ptslog && common_parser "nginx" "Connections:"
grep -q '^Redis' $ptslog && common_parser "pts/redis.*"
grep -q '^Apache HTTP Server' $ptslog && common_parser "Apache HTTP Server" "Concurrent Requests:"
grep -q '^PHPBench' $ptslog && common_parser "PHPBench" "PHP Benchmark Suite:"



# clean up temp dirs
#echo $tmpdir
#exit
rm -rf /tmp/ptslogs.*
