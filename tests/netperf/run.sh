. $TONE_ROOT/lib/cpu_affinity.sh

[[ ! ${NUMACTL} ]] && glb_numa='numactl -C 0 -m 0' || glb_numa=${NUMACTL}

setup()
{
    export PATH="$TONE_BM_RUN_DIR"/bin:$PATH

    [ -n "$send_size" ] && test_options="-- -m $send_size"

    firewalld_status_flag=0
    firewalld_status=`systemctl status firewalld`
    if [[ $firewalld_status =~ "active" ]];then
        systemctl stop firewalld
        firewalld_status_flag=1
    fi

     opt_ip=
    [ "$IP" = 'ipv4' ] && opt_ip='-4'
    [ "$IP" = 'ipv6' ] && opt_ip='-6'

    [ -n "$SERVER" ] && server=${SERVER%% *} || server=localhost
    echo "Run netserver on host: $server"

    if [ "$server" = localhost ]; then
        source "$TONE_BM_SUITE_DIR"/netserver.sh
    else
        ssh $server "export 'glb_numa=${glb_numa}'; TONE_ROOT=$TONE_ROOT TONE_BM_RUN_DIR=$TONE_BM_RUN_DIR IP=$IP server=$server $TONE_BM_SUITE_DIR/netserver.sh"
    fi
}

run()
{
    base_cmd="netperf $opt_ip -t $test -c -C -l $runtime -H $server $test_options"

    logger ${glb_numa} $base_cmd &
    logger wait
}

teardown()
{
    if [ "$server" = localhost ]; then
        pkill netserver
    else
        ssh $server pkill netserver
    fi
    if [ $firewalld_status_flag -eq 1 ];then
        systemctl start firewalld
    fi

}

parse()
{
    awk -f "$TONE_BM_SUITE_DIR"/parse.awk
}

